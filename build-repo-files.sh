#!/bin/sh

if [ $# -ne 1 ]; then
   echo Exatcly one argument -- the repo dir expected 1>&2
   exit 1
fi


REPO_DIR="$1"

cd "$REPO_DIR"

mkdir -p  "dists/stable/main/binary-amd64"
mkdir -p  "dists/stable/main/binary-arm64"
mkdir -p  "dists/stable/main/binary-all"

rm -f packages-i386.db packages-amd64.db packages-arm64.db
apt-ftparchive generate -c=aptftp.conf aptgenerate.conf

apt-ftparchive release -c=aptftp.conf dists/stable > dists/stable/Release

gpg -u $DEBEMAIL -bao dists/stable/Release.gpg dists/stable/Release
gpg -u $DEBEMAIL --clear-sign --output dists/stable/InRelease dists/stable/Release

